<?php

return [
    '\Selene\Packages\Framework\FrameworkPackage',
    '\Selene\Packages\Twig\TwigPackage',
    '\Selene\Packages\Doctrine\DoctrinePackage',
    '\Selene\Packages\Admin\AdminPackage',
];
