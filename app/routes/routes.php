<?php

use \Selene\Components\Routing\RouteBuilder;

$builder = new RouteBuilder;

$builder->routeGet('index', '/{name?}', ['_action' => 'App\Controllers\FooController:indexAction']);
