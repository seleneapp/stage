<?php

include_once __DIR__ . '/../../vendor/autoload.php';
include_once __DIR__ . '/autoload.php';

use \Selene\Components\Kernel\Environment;
use \Selene\Components\Kernel\Application;

return call_user_func(function () {

    $appPath = dirname(dirname(__DIR__)) . '/app';

    /*
     | Initialize the application
     */
    $app = new Application(
        $env = (new Environment(require __DIR__.'/env.php', $_SERVER))->detect(),
        'production' !== $env
    );

    /*
     | Set the application root path;
     */
    $app->setApplicationRoot($appPath);

    /*
     | Set the application cache path;
     */
    $app->setContainerCachePath($appPath . '/cache/container');

    /*
     | add paths for packageproviders;
     */
    $app->addPackageProvider($appPath . '/packages/framework');
    $app->addPackageProvider($appPath . '/packages/extra');
    $app->addPackageProvider($appPath . '/packages/workspace');

    return $app;
});

