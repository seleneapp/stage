<?php

return [
    'development' => [
        '*.dev',
    ],
    'production' => [
        '*.com',
    ]
];
