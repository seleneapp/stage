<?php

require_once __DIR__.'/../routes/routes.php';




use \Selene\Components\Routing\Router;
use \Selene\Components\DI\Reference;
use \Selene\Components\DI\Loader\XmlLoader;
use \Selene\Components\DI\BaseContainer as Container;
use \Selene\Components\DI\Definition\ServiceDefinition as Definition;

define('TEMPLATE_ROOT', __DIR__.'/../views');

$container = new Container;
$container->inject('app.container', $container);
$loader = new XmlLoader($container);
$loader->load(__DIR__.'/services.xml');

//$container->setParameter('kernel.class', 'Selene\Components\Kernel\Kernel');
//$container->setParameter('request_stack.class', 'Selene\Components\Http\RequestStack');
//$container->setParameter('event_dispatcher.class', 'Selene\Components\Events\Dispatcher');
//$container->setParameter('router.class', 'Selene\Components\Routing\Router');
//$container->setParameter('controller_resolver.class', 'Selene\Components\Routing\Controller\Finder');
//$container->setParameter('filesystem.class', 'Selene\Components\Filesystem\Filesystem');
//$container->setParameter('twig_env.class', 'Twig_Environment');
//$container->setParameter('twig_fileloader.class', 'Selene\Adapters\Twig\Loaders\FileLoader');
//$container->setParameter('twig_engine.class', 'Selene\Adapters\Twig\TwigEngine');
//$container->setParameter('view_env.class', 'Selene\Components\View\Environment');
//$container->setParameter('template_root', __DIR__.'/../views');
//
//$container->setDefinition(
//    'filesystem',
//    new Definition(
//        '%filesystem.class%',
//        []
//    )
//);
//
//$container->setDefinition(
//    'request_stack',
//    new Definition(
//        '%request_stack.class%'
//    )
//);
//
//$container->setDefinition(
//    'app_kernel.stack',
//    new Definition(
//        '%kernel.class%',
//        ['$app.events', '$app.router']
//    )
//);
//
//$container->setDefinition(
//    'app.events',
//    new Definition(
//        '%event_dispatcher.class%',
//        ['$app.container']
//    )
//);
//
//$container->setDefinition(
//    'controller_resolver',
//    new Definition(
//        '%controller_resolver.class%',
//        ['$app.container']
//    )
//);
//
$container->setDefinition(
    'app.router',
    new Definition(
        '%router.class%',
        ['$controller_resolver']
    )
)
->addSetter('setRoutes', [$builder->getRoutes()])
->addSetter('setEventDispatcher', ['$app.events']);

//$container->setDefinition(
//    'twig_env',
//    new Definition(
//        '%twig_env.class%',
//        ['$twig_fileloader', ['debug' => true, 'cache' => __DIR__.'/../cache/twig']]
//    )
//);

//// view
//$container->setDefinition(
//    'twig_engine',
//    new Definition(
//        '%twig_engine.class%',
//        ['$twig_env']
//    )
//);
//
//$container->setDefinition(
//    'twig_fileloader',
//    new Definition(
//        '%twig_fileloader.class%'
//    )
//);
//
//$container->setDefinition(
//    'twig_env',
//    new Definition(
//        '%twig_env.class%',
//        ['$twig_fileloader', ['debug' => true, 'cache' => __DIR__.'/../cache/twig']]
//    )
//);
//
//$container->setDefinition(
//    'view',
//    new Definition(
//        '%view_env.class%',
//        ['%template_root%']
//    )
//)->addSetter('registerEngine', ['$twig_engine']);
//$container->get('twig_engine');
